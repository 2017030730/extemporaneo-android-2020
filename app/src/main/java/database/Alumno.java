package database;

import java.io.Serializable;

public class Alumno implements Serializable {
    private long id;
    private String matricula;
    private String calificacion;
    private String materia;
    private String grupo;

    public Alumno() {
    }

    public Alumno(long id, String matricula, String calificacion, String materia, String grupo) {
        this.id = id;
        this.matricula = matricula;
        this.calificacion = calificacion;
        this.materia = materia;
        this.grupo = grupo;
    }

    public Alumno(String matricula, String calificacion, String materia, String grupo) {
        this.matricula = matricula;
        this.calificacion = calificacion;
        this.materia = materia;
        this.grupo = grupo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

}
