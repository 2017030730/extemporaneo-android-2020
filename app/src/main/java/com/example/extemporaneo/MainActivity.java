package com.example.extemporaneo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private final static String[] grupos = { "GRUPO", "01", "02", "03" };
    private Spinner cmbGrupos;
    private Button btnSalir;
    private Button btnRegistrar;
    private String selecc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSalir = (Button) findViewById(R.id.btnSalir);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, grupos);
        cmbGrupos = (Spinner) findViewById(R.id.cmbGrupos);
        cmbGrupos.setAdapter(adapter);
        cmbGrupos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selecc = cmbGrupos.getSelectedItem().toString(); //Recuperamos el texto del elemento seleccionado
                //Toast.makeText(getApplicationContext(), ""+selecc, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selecc.equals("GRUPO")) {
                    Toast.makeText(MainActivity.this, "Por favor seleccione un grupo", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(MainActivity.this, RegistrarCalif.class);

                    //Enviar un dato String
                    intent.putExtra("selecc", selecc);

                    startActivity(intent);
                }

            }
        });

    }

}