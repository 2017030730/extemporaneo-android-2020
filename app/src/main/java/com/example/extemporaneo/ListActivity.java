package com.example.extemporaneo;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.Alumno;
import database.CalifAlumno;

public class ListActivity extends AppCompatActivity {
    private ListView listView;
    private CalifAlumno califDB;
    private MyArrayAdapter adapter;
    private ArrayList<Alumno> listaCalif;
    private String grupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Bundle datos = getIntent().getExtras();
        grupo = datos.getString("grupo");

        listView = (ListView)findViewById(R.id.list);
        califDB = new CalifAlumno(this);
        listaCalif = new ArrayList<Alumno>();
        llenarLista();
        adapter = new MyArrayAdapter(this,R.layout.layout_calif, listaCalif);
        listView.setAdapter(adapter);
    }

    public void llenarLista() {
        califDB.openDatabase();
        listaCalif = califDB.allCalifAlumnos(grupo);
        califDB.cerrar();
        if(listaCalif.size() > 0){
            Toast.makeText(ListActivity.this,"Si hay : " + listaCalif.size(),Toast.LENGTH_SHORT).show();
        }
    }


    class MyArrayAdapter extends ArrayAdapter<Alumno> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Alumno> alumnoArrayList;
        private LayoutInflater inflater;

        public MyArrayAdapter(@NonNull Context context, @LayoutRes int textViewResourceId, ArrayList<Alumno> datosAlumno) {
            super(context, textViewResourceId, datosAlumno);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.alumnoArrayList = datosAlumno;
            this.inflater =(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceId,parent,false);
            TextView lblMatr = (TextView) view.findViewById(R.id.lblMatr);
            TextView lblMate = (TextView) view.findViewById(R.id.lblMate);
            TextView lblCal = (TextView) view.findViewById(R.id.lblCal);

            lblMatr.setText(alumnoArrayList.get(position).getMatricula());
            lblMate.setText(alumnoArrayList.get(position).getMateria());
            lblCal.setText(alumnoArrayList.get(position).getCalificacion());

            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return super.getDropDownView(position, convertView, parent);
        }
    }
}